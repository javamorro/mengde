/**
 * Klassen Mengde
 * 
 * Jeg har valgt aa ha en dobbelt lenket liste. Dette betyr at alle 
 * Nodeobjektene blir pekt paa av baade neste og forrige objektet. 
 * Fordelen med det, er at jeg slipper aa gaa gjennom hele stacken for aa komme til 
 * forste/siste Nodeobjektet (Spesielt stor fordel naar stacken er ekstremt lang). 
 * Jeg slipper ogsaa aa iterere med flere while lokker.
 * Ulempen er at det er litt mer komplisert aa legge til objekter.
 * 
 * @author vegardds
 */

public class Mengde<E> {
	private Node hode;
	private Node hale;

	public Mengde(){
		hode = new Node(null);
		hale = new Node(null);
		hode.neste = hale;
		hale.forrige = hode;
	}

	private class Node {
		E objekt;
		Node neste;
		Node forrige;

		Node(E objekt) {
			this.objekt = objekt;
		}
	}

	public boolean tom() {
		return hode.neste == hale; 
	}

	public boolean leggTil(E e) {
		if (inneholder(e)) {
			return false;
		}
		Node tmp = new Node(e);     //Lager en midlertidig node: tmp
		tmp.forrige = hale.forrige; //Gir tmp en forrige peker
		hale.forrige.neste = tmp;   //Sorger for at tmp blir pekt paa
		tmp.neste = hale;           //Gir tmp en nestepeker
		hale.forrige = tmp;         //hale objektet peker paa tmp

		return true;
	}

	public E fjernEldste() {
		if (tom()) {
			return null;
		}
		Node tmp = hode.neste;    //tmp settes til det forste objektet som ble satt inn
		tmp.neste.forrige = hode; //Nest eldste peker paa hodet
		hode.neste = tmp.neste;   //hode peker paa nest eldste

		return tmp.objekt;
	}

	public E fjernNyeste() {
		if (tom()) {
			return null;
		}
		Node tmp = hale.forrige;    //tmp settes til det siste objektet som ble satt inn
		tmp.forrige.neste = hale;   //Nest nyeste peker paa hale
		hale.forrige = tmp.forrige; //hale peker paa nest nyeste

		return tmp.objekt;
	}

	public boolean inneholder(E e) {
		
		Node tmp = hode;

		while (tmp.neste != null) {
			if (tmp.objekt == e) {
				return true;
			}
			tmp = tmp.neste;
		}
		return false;
	}
}